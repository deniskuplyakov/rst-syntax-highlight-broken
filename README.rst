=============================================================================
ReStructuredText has broken syntax highlight for code-blocks in rendered view
=============================================================================

.. code-block:: python

   py = some.magic()

   def foo():
       return "bar"
